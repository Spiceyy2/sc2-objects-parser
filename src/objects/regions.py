import re
import utils.regex as re_utils
import utils.object as obj_utils
import utils.string as str_utils

class quad:
    _RE_QUAD = re.compile(rf'({re_utils.FP_RE}),({re_utils.FP_RE}),({re_utils.FP_RE}),({re_utils.FP_RE})')
    def __init__(self, top:float=None, left:float=None, bottom:float=None, right:float=None):
        self.top    = top
        self.left   = left
        self.bottom = bottom
        self.right  = right

    @classmethod
    def from_xml(cls, elem):
        quad_elem = elem.find('quad')
        if quad_elem == None:
            raise RuntimeError('Invalid quad quad')

        qd = quad_elem.get('value')
        m = re.match(quad._RE_QUAD, qd)

        if m is None:
           print(f'Invalid quad format {qd}')
           return

        top    = float(m.group(1))
        left   = float(m.group(2))
        bottom = float(m.group(3))
        right  = float(m.group(4))

        return cls(top, left, bottom, right)

    def __str__(self):
        return f'Q,{str_utils.strfmt_compact_f(self.top)},{str_utils.strfmt_compact_f(self.left)},{str_utils.strfmt_compact_f(self.bottom)},{str_utils.strfmt_compact_f(self.right)}'

class circle:
    _RE_CENTER = re.compile(rf'({re_utils.FP_RE}),({re_utils.FP_RE})')
    def __init__(self, cx:float=None, cy:float=None, r:float=None):
        self.cx   = cx
        self.cy   = cy
        self.r    = r

    @classmethod
    def from_xml(cls, elem):
        elem_center = elem.find('center')
        if elem_center == None:
            raise RuntimeError('Invalid circle center')

        center = elem_center.get('value')
        m = re.match(circle._RE_CENTER, center)
        if m is None:
            print(f'Invalid circle center format {center}')
            return

        cx = float(m.group(1))
        cy = float(m.group(2))

        elem_rad = elem.find('radius')
        if elem_rad == None:
            raise RuntimeError('Invalid circle radius')

        rad = elem_rad.get('value')

        r = float(rad)

        return cls(cx, cy, r)

    def __str__(self):
        return f'C,{str_utils.strfmt_compact_f(self.cx)},{str_utils.strfmt_compact_f(self.cy)},{str_utils.strfmt_compact_f(self.r)}'

class region:
    def __init__(self, id:str=None, name:str=None):
        self.id          = id
        self.name        = name
        self.shapes      = []

    def from_xml(self, elem):
        self.id    = elem.get('id')
        self.name  = elem.find('name')
        if self.name == None:
            self.name = obj_utils.IDENT_DEF
        else:
            self.name = self.name.get('value')

        for shape in elem.findall('shape'):
            type = shape.get('type')
            if type == 'circle':
                shape_obj = circle.from_xml(shape)
                self.shapes.append(shape_obj)
            elif type == 'rect':
                shape_obj = quad.from_xml(shape)
                self.shapes.append(shape_obj)
            elif type == "diamond":
                print('Warning. Unsupported region type diamond.')
            else:
                print(f'Invalid region type {type}')

    def __str__(self):
        res = f'{self.id},{self.name},'
        res += ",".join(str(shape) for shape in self.shapes)
        return res