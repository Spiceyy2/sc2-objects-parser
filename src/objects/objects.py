from __future__ import annotations
import xml.etree.ElementTree as ET
import re
import utils.math as math_utils
import utils.regex as re_utils
import utils.object as obj_utils
import utils.string as str_utils

HDR_DEF   = 1
ANGLE_DEF = 0

OBJ_FLAGS = {
        'HeightOffset'      : 1 << 0,
        'HeightAbsolute'    : 1 << 1,
        'ForcePlacement'    : 1 << 2,
        'NoDoodadFootprint' : 1 << 3,
        'DisableNoFlyZone'  : 1 << 4
}

UNIT_FLAGS = {
        'HeightOffset'      : 1 << 0,
        'HeightAbsolute'    : 1 << 1,
        'ForcePlacement'    : 1 << 2,
        'UnitHidden'        : 1 << 3,
        'DeferModelLoad'    : 1 << 4
}

def elem_to_flags(elem, flags_dict:dict):
    flags = 0
    for flag_elem in elem:
        idx = flag_elem.get('Index')
        val = flag_elem.get('Value')
        if val == "1":
            flags |= flags_dict[idx]

    return flags
class object_list:
    def __init__(self, doodads=None, units=None, points=None, noflyzones=None, regions=None):
        self.__doodads    = doodads
        self.__units      = units
        self.__points     = points
        self.__noflyzones = noflyzones
        self.__regions    = regions

    def set_doodads(self, doodads:list):
        self.__doodads = doodads
    def set_units(self, units:list):
        self.__units = units
    def set_points(self, points:list):
        self.__points = points
    def set_noflyzones(self, noflyzones:list):
        self.__noflyzones = noflyzones
    def set_regions(self, regions:list):
        self.__regions = regions

    def merge(self, obj_list:object_list):
        self.__doodads.extend(obj_list.__doodads)
        self.__units.extend(obj_list.__units)
        self.__points.extend(obj_list.__points)
        self.__noflyzones.extend(obj_list.__noflyzones)
        self.__regions.extend(obj_list.__regions)

    def doodads_as_str(self)->str:
        return ';'.join((str(obj) for obj in self.__doodads))
    def units_as_str(self)->str:
        return ';'.join((str(obj) for obj in self.__units))
    def points_as_str(self)->str:
        return ';'.join((str(obj) for obj in self.__points))
    def noflyzones_as_str(self)->str:
        return ';'.join((str(obj) for obj in self.__noflyzones))
    def regions_as_str(self)->str:
        return ';'.join((str(obj) for obj in self.__regions))

class doodad:
    _RE_HDR = re.compile(r'(\d+,\d+,\d+) (\d+(?:.\d+)?)')

    def __init__(self, id:str=None, name:str = None, type:str=None, var:str=None, scale:str=None, pos:str=None, \
    rot:str=None, pitch:str = None, roll:str = None, tint_color:str = None, team_color:str = None, flags:int=0):
        self.id    = id
        self.name  = name

        self.type  = type
        self.var   = var
        
        self.scale = scale
        self.pos   = pos
        self.rot   = rot
        self.pitch = pitch
        self.roll  = roll
        self.tint_color = tint_color
        self.team_color = team_color
        self.flags = flags

    @classmethod
    def from_xml(cls, elem):
        id          = elem.get('Id')
        name        = elem.get('Name', obj_utils.IDENT_DEF)

        type        = elem.get('Type')
        var         = elem.get('Variation', "0")

        scale       = elem.get('Scale', obj_utils.IDENT_DEF)
        pos         = elem.get('Position', obj_utils.IDENT_DEF)
        rot         = elem.get('Rotation', obj_utils.IDENT_DEF)
        pitch       = elem.get('Pitch', obj_utils.IDENT_DEF)
        roll        = elem.get('Roll', obj_utils.IDENT_DEF)
        tint_color  = elem.get('TintColor', obj_utils.IDENT_DEF)
        team_color  = elem.get('TeamColor', obj_utils.IDENT_DEF)
        flags       = elem_to_flags(elem, OBJ_FLAGS)

        if tint_color != obj_utils.IDENT_DEF:
            m = re.match(doodad._RE_HDR, tint_color)
            if m is None:
                tint_color += f',{HDR_DEF}'
            else:
                hdr = m.group(2)
                if hdr == None:
                    tint_color += f',{HDR_DEF}'
                else:
                    tint_color = f'{m.group(1)},{str_utils.strfmt_compact(hdr)}'

        if pitch != obj_utils.IDENT_DEF or roll != obj_utils.IDENT_DEF:
            if rot == obj_utils.IDENT_DEF:
                rot = str(ANGLE_DEF)
            if pitch == obj_utils.IDENT_DEF:
                pitch = str(ANGLE_DEF)
            if roll == obj_utils.IDENT_DEF:
                roll = str(ANGLE_DEF)

        rot   = math_utils.normalize_actor_angle_str(rot, obj_utils.IDENT_DEF) 
        pitch = math_utils.normalize_actor_angle_str(pitch, obj_utils.IDENT_DEF) 
        roll  = math_utils.normalize_actor_angle_str(roll, obj_utils.IDENT_DEF)

        return cls(id, name, type, var, scale, pos, rot, pitch, roll, tint_color, team_color, flags)



    # more performant way to parse in galaxy
    def __str__(self):
        res = f'{self.id},{self.name},{self.type},{self.var},{self.scale},{self.pos},{self.rot},{self.pitch},{self.roll},{self.tint_color},{self.team_color}'
        if self.flags != 0:
            res += f',{self.flags}'
        return res
    
    # def __str__(self):
    #     res = f'{self.id},{self.name},{self.type} {self.var} {self.scale} {self.pos} {self.rot},{self.pitch},{self.roll} {self.tint_color} {self.team_color}'
    #     if self.flags != 0:
    #         res += f' {self.flags}'
    #     return res
class unit:
    def __init__(self, id:str=None, name:str=None, type:str=None, owner:str=None, pos:str=None, rot:str=None, flags:int=0):
        self.id     = id
        self.name   = name
        self.type   = type
        self.owner  = owner
        self.pos    = pos
        self.rot    = rot
        self.flags  = flags

    @classmethod
    def from_xml(cls, elem):
        id     = elem.get('Id')
        name   = elem.get('Name', obj_utils.IDENT_DEF)
        type   = elem.get('UnitType')

        owner = elem.get('Player', 1)

        pos    = elem.get('Position', obj_utils.IDENT_DEF)
        rot    = elem.get('Rotation', ANGLE_DEF)
        flags  = elem_to_flags(elem, UNIT_FLAGS)

        rot    = math_utils.normalize_unit_angle_str(rot, obj_utils.IDENT_DEF) 

        return cls(id, name, type, owner, pos, rot, flags)

    def __str__(self):
        res = f'{self.id},{self.name},{self.type},{self.owner},{self.pos},{self.rot}'
        if self.flags != 0:
            res += f',{self.flags}'
        return res
class point:
    def __init__(self, id:str=None, name:str=None, type:str=None, pos:str=None, flags:int=0):
        self.id    = id
        self.name  = name
        self.type  = type
        self.pos   = pos
        self.flags = flags

    @classmethod
    def from_xml(cls, elem):
        id    = elem.get('Id')
        name  = elem.get('Name')
        type  = elem.get('Type')
        pos   = elem.get('Position', obj_utils.IDENT_DEF)
        flags = elem_to_flags(elem, OBJ_FLAGS)

        return cls(id, name, type, pos, flags)

    def __str__(self):
        res = f'{self.id},{self.name},{self.type},{self.pos}'
        if self.flags != 0:
            res += f',{self.flags}'
        return res

class noflyzone(point):
    def __init__(self, id:str=None, name:str=None, type:str=None, pos:str=None, path_hard:str=None, path_soft:str=None, flags:int=0):
        super().__init__(id, name, type, pos, flags)
        self.path_soft = path_soft
        self.path_hard = path_hard

    @classmethod
    def from_xml(cls, elem):
        nfz = super().from_xml(elem)
        nfz.path_soft = elem.get('PathingRadiusSoft', 1)
        nfz.path_hard = elem.get('PathingRadiusHard', 1)

        return nfz

    def __str__(self):
        res = f'{self.id},{self.name},{self.pos},{str_utils.strfmt_compact_f(self.path_soft)},{str_utils.strfmt_compact_f(self.path_hard)}'
        if self.flags != 0:
            res += f',{self.flags}'
        return res