import xml.etree.ElementTree as ET
import sys
import os
from objects.objects import point,noflyzone,unit,doodad,object_list
from objects.regions import region,quad,circle
import argparse

VERSION  = 0.3

OBJ_FILENAME = 'Objects'
REG_FILENAME = 'Regions'

#parse Objects file to an object_list
def parse_objects(map_path:str, obj_list:object_list):
    in_et = ET.ElementTree(file=f'{map_path}/{OBJ_FILENAME}')
    in_root = in_et.getroot()

    doodads,units,points,noflyzones = [],[],[],[]

    for elem in in_root.findall("./"):
        if elem.tag == 'ObjectDoodad':
            obj = doodad.from_xml(elem)
            doodads.append(obj)
        elif elem.tag == 'ObjectPoint':
            type = elem.get('Type')
            if type == 'NoFlyZone':
                obj = noflyzone.from_xml(elem)
                noflyzones.append(obj)
            else:
                obj = point.from_xml(elem)
                points.append(obj)
        elif elem.tag == 'ObjectUnit':
            obj = unit.from_xml(elem)
            units.append(obj)
    
    obj_list.set_doodads(doodads)
    obj_list.set_units(units)
    obj_list.set_points(points)
    obj_list.set_noflyzones(noflyzones)

def parse_regions(map_path:str, obj_list:object_list):
    in_et = ET.ElementTree(file=f'{map_path}/{REG_FILENAME}')
    in_root = in_et.getroot()
    regions = []

    for elem in in_root.findall("./"):
        if elem.tag == 'region':
            obj = region()
            obj.from_xml(elem)
            regions.append(obj)
    
    obj_list.set_regions(regions)

#create and return UserData element
def ud_mkelem(parent, field_type:str, data:str):
    elem = ET.SubElement(parent, 'String', 
        attrib={'String': f'{data}'})
    ET.SubElement(elem, 'Field', attrib={'Id': f'{field_type}'})
    return elem

#output userdata from objects to filename
def output_userdata(args:list, objects:object_list):
    root = ET.Element('Catalog')
    out_et = ET.ElementTree(root)

    elem_cs = ET.SubElement(root, 'CUser', attrib={'id': f'{OBJ_FILENAME}'})
    elem_instance = ET.SubElement(elem_cs, 'Instances', attrib={'Id': f'Map'})

    ET.SubElement(elem_cs, 'Fields', attrib={'Id': 'Version', 'Type': 'String'})
    ud_mkelem(elem_instance, 'Version', f'{VERSION}')

    if args.doodads:
        ET.SubElement(elem_cs, 'Fields', attrib={'Id': 'Doodads', 'Type': 'String'})
        ud_mkelem(elem_instance, 'Doodads', objects.doodads_as_str())
    if args.units:
        ET.SubElement(elem_cs, 'Fields', attrib={'Id': 'Units', 'Type': 'String'})
        ud_mkelem(elem_instance, 'Units', objects.units_as_str())
    if args.points:
        ET.SubElement(elem_cs, 'Fields', attrib={'Id': 'Points', 'Type': 'String'})
        ud_mkelem(elem_instance, 'Points', objects.points_as_str())
    if args.noflyzones:
        ET.SubElement(elem_cs, 'Fields', attrib={'Id': 'NoFlyZones', 'Type': 'String'})
        ud_mkelem(elem_instance, 'NoFlyZones', objects.noflyzones_as_str())
    if args.regions:
        ET.SubElement(elem_cs, 'Fields', attrib={'Id': 'Regions', 'Type': 'String'})
        ud_mkelem(elem_instance, 'Regions', objects.regions_as_str())

    ET.indent(out_et, space="\t", level=0)
    out_et.write(args.out, encoding="utf-8", xml_declaration=True)

def dir_path(path):
    if os.path.isdir(path):
        return path
    else:
        raise argparse.ArgumentTypeError(f"readable_dir:{path} is not a valid path")

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("map_path", type=dir_path, help="path to unpacked map")
    parser.add_argument("-out", "--out", type=argparse.FileType('wb'), default="UserData.xml", help="path to UserData output file")
    incl = parser.add_argument_group('include (at least one of these flags is required)', 'data that is serialized to userdata')
    incl.add_argument("-d", "--doodads", action="store_true", help="output doodads")
    incl.add_argument("-u", "--units", action="store_true", help="output units")
    incl.add_argument("-p", "--points", action="store_true", help="output points")
    incl.add_argument("-f", "--noflyzones", action="store_true", help="output noflyzones")
    incl.add_argument("-r", "--regions", action="store_true", help="output regions")
    args = parser.parse_args()

    if not (args.doodads or args.units or args.points or args.noflyzones or args.regions):
        parser.error('Must specify data to serialize (-d/-u/-p/-f/-r)')

    objects = object_list()
    if args.doodads or args.units or args.points or args.noflyzones:
        parse_objects(args.map_path, objects)
    if args.regions:
        parse_regions(args.map_path, objects)
    output_userdata(args, objects)

if __name__ == "__main__":
    main()