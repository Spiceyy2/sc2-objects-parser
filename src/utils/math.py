import utils.string as str_utils
import math

# restrict angle in degrees to within -180 < angle <= 180 
def angle180(angle:float) -> float:
    # reduce the angle  
    angle = angle % 360

    # force it to be the positive remainder, so that 0 <= angle < 360  
    angle = (angle + 360) % 360 

    # force angle within range -180 < angle <= 180  
    if angle > 180:
        angle -= 360

    return angle

#convert from radians in Sc2's terrain coordinate system to degrees in actor coordinate system
def rad_to_degrees(angle:float) -> float:
    # subtract by pi first to get the angle within -pi < angle <= pi
    # multiply by -180 because coordinate system is backwards
    return (angle - math.pi) / math.pi * -180.0 + 180.0

#convert from radians to degrees and restrict angle to within -180 < angle <= 180 (for use with actor messages)
def normalize_angle_actor(angle:float) -> float:
    return angle180(rad_to_degrees(angle))

#convert from radians to degrees and restrict angle to within 0 <= angle < 360 (for use with unit facing)
def normalize_angle_unit(angle:float) -> float:
    return math.degrees(angle) - 90

#convert from radians to degrees and restrict angle to within -180 < angle <= 180 
def normalize_actor_angle_str(angle:str, ident_def:str) -> str:
    if angle != ident_def:
        angle = str_utils.strfmt_compact(f'{normalize_angle_actor(float(angle)):.4f}')

    return angle
    
#convert from radians to degrees and restrict angle to within -180 < angle <= 180 
def normalize_unit_angle_str(angle:str, ident_def:str) -> str:
    if angle != ident_def:
        angle = str_utils.strfmt_compact(f'{normalize_angle_unit(float(angle)):.4f}')

    return angle
