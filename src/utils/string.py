def strfmt_compact(s:str) -> str:
    t = s.split('.') # handle case without a decimal point
    if len(t) < 2:
        return s
    
    return s.rstrip('0').rstrip('.')

def strfmt_compact_i(s:str, ignore:str) -> str:
    return strfmt_compact(s) if s != ignore else s

def strfmt_compact_f(s:float) -> str:
    return strfmt_compact(str(s))