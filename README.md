# README #
# SC2 Object Parser

* Objects parser for SC2 to allow iteration of points,doodads,preplaced units, no-fly-zones, and regions at runtime. This parser converts Objects file and Regions file into userdata that can be parsed with galaxy script. Comes with a very simple interface to parse and use all objects contained within the Objects and Regions file. 

* Version 0.3

# Getting started

* Install python and download the repository
* Run the python script pointing at the Objects file
* Copy the map to UserData.xml. The Field Id's only need to be in there once
 * Alternatively you may setup a dataspace to keep things organized.
 * Repeat for as many maps as you desire.
* Setup your map to parse the generated UserData.xml
* It is strongly encouraged to have a separate map file saved as components for each map containing terrain objects you wish to create dynamically. This allows for easy modification of the map.

## Installation
* Install python 3 (built using python 3.96)
* Run the python script in the terimal/command prompt/powershell (there is also a batch file that can be modified to run the script)

## Usage
```
usage: s2obj.py [-h] [-out OUT] [-d] [-u] [-p] [-f] [-r] map_path

positional arguments:
  map_path             path to unpacked map

optional arguments:
  -h, --help           show this help message and exit
  -out OUT, --out OUT  path to UserData output file

include (at least one of these flags is required):
  data that is serialized to userdata

  -d, --doodads        output doodads
  -u, --units          output units
  -p, --points         output points
  -f, --noflyzones     output noflyzones
  -r, --regions        output regions
```

## Example Map with Galaxy API
**V0.2** https://bitbucket.org/Spiceyy2/sc2-objects-parser/downloads/dynamic-objects-test_v0.2.SC2Map.zip

**V0.3** https://bitbucket.org/Spiceyy2/sc2-objects-parser/downloads/dynamic-objects-test_v0.3.SC2Map.zip

**V0.4** https://bitbucket.org/Spiceyy2/sc2-objects-parser/downloads/dynamic-objects-test_v0.4.SC2Map.zip

### Parsing UserData.xml from SC2 galaxy script
In the example map, UserData parsing is parsed under the hood in `PPObjects.galaxy` and a simple interface is exposed in `PPObjects_h.galaxy`.

```cpp
include "Script/Source/PPObjects"

void ParseObjects() {
  gs_PPDoodad ppDoodad;
  gs_PPPoint ppPoint;
  gs_PPNoFlyZone ppNoFlyZone;
  gs_PPUnit ppUnit;
  gs_PPRegion ppRegion;

  //It is recommended to save a different map file for each map you plan to dynamically switch to. This allows for easy modifications.
  //"Map" is the mapname in UserData.xml. You can create many maps there and all you have to do is load a new map and iterate through the doodads to create them.
  PPLoadMap("Map");
  while(PPNextDoodad(ppDoodad)) {
    PPDoodadCreate(ppDoodad);
    // Can also store these doodads in an array at map init if you need to create doodads super fast 
    // ... at runtime but this is a niche usecase and performance seems to be good enough with moderate amounts of doodads without doing so.
    // UIDisplayMessage(PlayerGroupAll(), c_messageAreaChat, StringToText(ppDoodad.name));
  }
  while(PPNextUnit(ppUnit)) {
    PPUnitCreate(ppUnit);
  }
  while(PPNextPoint(ppPoint)) {
    PPPointCreate(ppPoint);
  }
  while(PPNextRegion(ppRegion)) {
    //do stuff with ppRegion or ppRegion.lv_reg
  }
  while(PPNextNoFlyZone(ppNoFlyZone)) {
    // do stuff with ppNoFlyZone
  }
}
```

### SC2 galaxy script interface
```cpp
gs_PPMap gv_ppMap;

void PPLoadMap(string mapName);

bool PPNextDoodad(PPDoodad_t out);
bool PPNextUnit(PPUnit_t out);
bool PPNextPoint(PPPoint_t out);
bool PPNextRegion(PPRegion_t out);
bool PPNextNoFlyZone(PPNoFlyZone_t out);

actor PPDoodadCreate(PPDoodad_t in);
unit PPUnitCreate(PPUnit_t in);
point PPPointCreate(PPPoint_t in);
```

### Useful structures
```cpp
struct gs_PPDoodad {
    int lv_props; //props are binary or'd together

    int lv_id;
    string lv_name;

    string lv_type;
    int lv_variation;

    fixed lv_scaleX;
    fixed lv_scaleY;
    fixed lv_scaleZ;

    fixed lv_posX;
    fixed lv_posY;
    fixed lv_posZ;

    fixed lv_rotation;
    fixed lv_pitch;
    fixed lv_roll;

    string lv_tint;
    fixed lv_tintHDR;

    int lv_teamclr_idx;

    int lv_flags; //flags are binary or'd together
};
typedef structref<gs_PPDoodad> PPDoodad_t;

struct gs_PPUnit {
    int lv_id;
    string lv_name;
    string lv_type;

    int lv_player;

    fixed lv_posX;
    fixed lv_posY;
    fixed lv_posZ;

    fixed lv_rotation;

    int lv_flags; //flags are binary or'd together
};
typedef structref<gs_PPUnit> PPUnit_t;

struct gs_PPPoint {
    int lv_id;
    string lv_name;
    string lv_type;

    fixed lv_posX;
    fixed lv_posY;
    fixed lv_posZ;

    int lv_flags; //flags are binary or'd together
};
typedef structref<gs_PPPoint> PPPoint_t;

struct gs_PPNoFlyZone {
    gs_PPPoint lv_pt;

    fixed lv_path_soft;
    fixed lv_path_hard;
};
typedef structref<gs_PPNoFlyZone> PPNoFlyZone_t;

struct gs_PPRegion {
    int lv_id;
    string lv_name;
    int lv_type;

    region lv_reg;
};
typedef structref<gs_PPRegion> PPRegion_t;

struct gs_PPInter {
    int lv_count;
    string lv_res;
};
typedef structref<gs_PPInter> PPInter_t;

struct gs_PPMap {
    string lv_mapName;
    string lv_version;

    gs_PPInter[PP_ITER_CNT] lv_iterPP;
};
typedef structref<gs_PPInter> PPMap_t;
```

### User Data Format (XML)
```xml
<?xml version='1.0' encoding='utf-8'?>
<Catalog>
	<CUser id="Objects">
		<Instances Id="Map">
			<String String="0.3">
				<Field Id="Version" />
			</String>
			<String String="627,@,Decal,3,1,0.75,1,64.5,193.5,8.489,-134.9889,@,@,@,@,7;1240448887,@,DecalProtoss,1,1,1.25,1,104,104.5,7.9868,-179.9546,@,@,@,@,7;966243685,@,DecalProtoss,4,1,1,1,64,86.5,7.9816,-179.9546,@,@,@,@,7;87,@,Pylon3,0,1.0454,1.0454,1.0454,107.5,135.5,11.881,103.0112,@,@,@,@,15;2122318860,@,DecalProtoss,0,1.5,1.5,1.5,101.5,43.5,7.9843,-134.9717,@,@,@,@,7;18,@,BraxisAlphaTallSign,0,1.25,1.5,1.25,20.5,194,8.2631,90.0166,@,@,@,@,7;">
				<Field Id="Doodads" />
			</String>
			<String String="697179794,@,GoalPost,1,14.5,34,0,-90,4;459329776,@,GoalPost,1,14.5,47,0,89.9832,4;1051639929,@,GoalPost,1,23.5,86,0,-90,4;1681559130,@,GoalPost,1,20.5,144.6198,0,89.9832,4;1007910734,@,GoalPost,1,113.5,34,0,-90,4;1858391984,@,GoalPost,1,104.5,86,0,-90,4;1400788830,@,GoalPost,1,104.5,96,0,90.0692,4;">
				<Field Id="Units" />
			</String>
			<String String="46,Aiur_P4,Normal,86.0397,150.2697,0;14,Aiur_P1,Normal,42.0097,150.2897,0;15,Aiur_P2,Normal,42.0097,127.8398,0;47,Avernus_P4,Normal,86,184,0;167,Blue Start,StartLoc,103.5,194.5,0;48,Avernus_P3,Normal,86,204,0;40,Aiur_P3,Normal,86.0397,127.7697,0;">
				<Field Id="Points" />
			</String>
			<String String="156,No Fly Zone 003,20.5,198,0,1,1;1640843857,No Fly Zone 077,20.24,85.3156,0,1,0.5;1977746745,No Fly Zone 018,24,86.5,0,1,1;1494379489,No Fly Zone 052,113.7385,44.8583,0,1,1;597881158,No Fly Zone 071,110.7658,132.7226,0,1,0.5;1416957209,No Fly Zone 072,108.4494,144.7924,0,1,0.5;1154417937,No Fly Zone 043,14.5393,37.0478,0,1,1;">
				<Field Id="NoFlyZones" />
			</String>
			<String String="1,Avernus.CenterSpawn,C,64,194,4;6,Avernus.LeftLine,Q,41.496,176,42.496,212;7,Avernus.RightLine,Q,85.5,176,86.5,212;9,Avernus.LeftGoal,Q,16,189,21,199;10,Avernus.RightGoal,Q,107,189,112,199;">
				<Field Id="Regions" />
			</String>
		</Instances>
		<Fields Id="Version" Type="String" />
		<Fields Id="Doodads" Type="String" />
		<Fields Id="Units" Type="String" />
		<Fields Id="Points" Type="String" />
		<Fields Id="NoFlyZones" Type="String" />
		<Fields Id="Regions" Type="String" />
	</CUser>
</Catalog>
```

#### Why UserData?

UserData allows the separation of data from code and makes it easy to load objects on demand saving memory in the galaxy vm.

#### Overhead

Performance using UserData in this compact format is actually pretty good as long as there is no need to create thousands of objects constantly in a lag-sensitive environment. UserData is often expressed as being very slow but this isn't as much of an issue with fewer XML keys. The parsing of UserData has been optimized to be fast even without caching doodads. However, if lag is of any concern it is very easy to cache all the doodads for a specific map in a galaxy array during map initialization or during voting.

While data size is not the most important thing it can matter if you have a lot of large maps that need saved. This format is very compact as compared with raw galaxy initialization and compared to UserData with each value having its own key.